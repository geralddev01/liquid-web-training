import { Header } from "../components/Header";
import "../styles/main.scss";

export default function Home() {
  return (
    <main>
      <Header />
    </main>
  );
}
