import Image from "next/image";
import logoImg from "@/assets/images/logoIcon.svg";
import heartIcon from "@/assets/images/heartIcon.svg";

import Link from "next/link";
import { headerLinks } from "@/utils";

export const Header = () => {

  const renderLinks = () => {
    return headerLinks.map(({ link, title }) => (
      <li className="Header-item">
        <Link className="Header-link" href={link}>
          {title}
        </Link>
      </li>
    ));
  };

  return (
    <header className="Header">
      <div className="Header-top">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
      </div>
      <div className="container Header-container">
        <Link href="/">
          <figure className="Header-logo">
            <Image src={logoImg} width="auto" alt="logo" />
          </figure>
        </Link>
        <div className="Header-row">
          {/* <nav className="Header-menu">
            <ul className="Header-links">{renderLinks()}</ul>
          </nav> */}
          <figure>
            <Image src={heartIcon} width="auto" alt="heart icon" />
          </figure>
        </div>
      </div>
    </header>
  );
};
