export const headerLinks = [
  {
    title: "Servicios",
    link: "/servicios",
  },
  {
    title: "Productos",
    link: "/productos",
  },
  {
    title: "Bodas",
    link: "/bodas",
  },
  {
    title: "Estilos",
    link: "/estilos",
  },
  {
    title: "Blog",
    link: "/blog",
  },
];
